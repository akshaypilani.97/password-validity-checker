# password-validity-checker

Assignment for Application Developer profile in Magicpin

Checks for validity of password according to the following rules:
1. Minimum length: 6
2. Maximum length: 12
3. At least 1 letter in [a-z], [0-9], [A-Z] and [*$_#=@] each.
4. It should not contain any letter from [%!)(]

Instructions for running:

1. Use Python 3.6 to execute.
2. Run using the following command
> python password_validity_check.py
3. Program will accept comma separated passwords as input.