# function for check validity of given comma separated passwords
def check_password_validity(password):
	
	# set from which atleast one element should be present in a valid password
	includeSet = ['*','$','_','#','=','@']

	# set form no element should be present in a valid password
	excludeSet = ['%','!',')','(']

	hasAtleastOneLowercase = 0
	hasAtleastOneUppercase = 0
	hasAtleastOneDigit = 0
	hasAtleastFromIncludeSet = 0
	containsExcludeSetCharacters = 0
	tooSmall = 0
	tooLarge = 0

	# if length is less than minimum length
	if len(password) < 6:
		tooSmall = 1

	# if length is greater than maximum length	
	elif len(password) > 12:
		tooLarge = 1
		
	# iterating over each character in password
	for k in password:
		
		# character k is lowercase
		if k >= 'a' and k <= 'z':
			hasAtleastOneLowercase = 1

		# character k is uppercase	
		if k >= 'A' and k <= 'Z':
			hasAtleastOneUppercase = 1

		# character k is digit	
		if k >= '0' and k <= '9':
			hasAtleastOneDigit = 1

		# character k is a character from includeSet	
		if k in includeSet:
			hasAtleastFromIncludeSet = 1

		# character k is a character from excludeSet	
		if k in excludeSet:
			containsExcludeSetCharacters = 1

	if tooSmall == 1:
		print(password, "Failure Password must be at least 6 characters long.")

	elif tooLarge == 1:
		print(password, "Failure Password must be at max 12 characters long.")

	elif hasAtleastOneLowercase == 0:
		print(password, "Failure Password must contain at least one letter from a-z.")

	elif hasAtleastOneUppercase == 0:
		print(password, "Failure Password must contain at least one letter from A-Z.")

	elif hasAtleastOneDigit == 0:
		print(password, "Failure Password must contain at least one letter from 0-9.")

	elif hasAtleastFromIncludeSet == 0:
		print(password, "Failure Password must contain at least one letter from *$_#=@.")

	elif containsExcludeSetCharacters == 1:
		print(password, "Failure Password cannot contain %!)(.")

	else:
		print(password, "Success")

if __name__ == "__main__":

	# take comma separated passwords as input
	commaSeparatedPasswords = input()

	# split given comma separated passwords into a list of passwords
	passwords = commaSeparatedPasswords.split(",")

	for password in passwords:

		# calling check_password_validity() function to check password validity
		check_password_validity(password)